﻿using Dapper;
using System.Configuration;
using System.Data.Common;
using System.Data.SqlClient;

namespace DataSync.DataServer
{
    public class DbContext
    {
        public static DbConnection OpenConnection()
        {
            var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DbConn"].ConnectionString);
            connection.Open();
            return connection;
        }

        public static int SaveOrderDetail(string insertCmd)
        {
            using (var conn = OpenConnection())
            {
                return conn.Execute(insertCmd);
            }
        }

        public static string GetLastKey(string clientId, string tableName, string clientIdColumn, string keyColumn)
        {
            using (var conn = OpenConnection())
            {
                var cmd = $"select top 1 {keyColumn} from [dbo].[{tableName}] WHERE {clientIdColumn}='{clientId}' order by {keyColumn} desc";
                return conn.ExecuteScalar<string>(cmd);
            }
        }
    }
}