﻿using DataSync.Data;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web.Http;
using System.Xml;
using System.Xml.Serialization;

namespace DataSync.DataServer
{
    [RoutePrefix("api/clients")]
    public class ClientsController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(ClientsController));

        [HttpPost]
        [Route("{clientId}")]
        public IHttpActionResult SaveOrders(string clientId, [FromBody] List<dynamic> orders)
        {
            Log.Info($"Orders recieved from Client:{clientId} with count:{orders.Count}");

            var path = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);

            var xmlSerializer = new XmlSerializer(typeof(ClientConfig));
            if (!(xmlSerializer.Deserialize(XmlReader.Create(Path.Combine(path, "MappingConfig.xml"))) is ClientConfig
                result))
                return InternalServerError();

            var resultClients = result.Clients;
            var config = resultClients.FirstOrDefault(q => q.ClientId == clientId);

            try
            {
                foreach (var jObject in orders)
                {
                    foreach (var configItem in config.Items)
                    {
                        configItem.ClientValue = jObject[configItem.Key].ToString();
                    }
                    DbContext.SaveOrderDetail(config.InsertCmd);
                }
            }
            catch (Exception ex)
            {
                Log.Error("", ex);
                return InternalServerError(ex);
            }
            return Ok();
        }

        [Route("{clientId}")]
        public IHttpActionResult GetClients(string clientId)
        {
            var path = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);

            var xmlSerializer = new XmlSerializer(typeof(ClientConfig));
            if (!(xmlSerializer.Deserialize(XmlReader.Create(Path.Combine(path, "MappingConfig.xml"))) is ClientConfig
                result))
                return InternalServerError();

            var resultClients = result.Clients;
            var config = resultClients.FirstOrDefault(q => q.ClientId == clientId);

            if (config == null)
                return (IHttpActionResult)NotFound();
            else if (config.Items.All(q => q.Value != $"'{config.ClientId}'"))
            {
                Log.Error("Please mapping client id to sql server!!!");
                return InternalServerError(new Exception("Please mapping client id to sql server!!!"));
            }
            else
            {
                var tableName = ConfigurationManager.AppSettings["ServerTableName"];
                var clientIdColumn = ConfigurationManager.AppSettings["ClientIdColumn"];

                if (string.IsNullOrEmpty(tableName) || string.IsNullOrEmpty(clientIdColumn))
                {
                    Log.Error("Server table name can't be empty!!!");
                    return InternalServerError();
                }

                try
                {
                    config.LastKey = int.Parse(DbContext.GetLastKey(clientId, tableName, clientIdColumn, config.KeyMapName) ?? "0");
                }
                catch (Exception e)
                {
                    Log.Error("", e);
                    return InternalServerError(e);
                }
                return Ok(config);
            }
        }

        public IHttpActionResult GetClients()
        {
            var path = Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);

            var xmlSerializer = new XmlSerializer(typeof(ClientConfig));
            if (!(xmlSerializer.Deserialize(XmlReader.Create(Path.Combine(path, "MappingConfig.xml"))) is ClientConfig
                result))
                return InternalServerError();

            var resultClients = result.Clients;
            return Ok(resultClients);
        }
    }
}