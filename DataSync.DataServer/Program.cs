﻿using log4net;
using log4net.Config;
using System;
using System.ServiceProcess;

namespace DataSync.DataServer
{
    internal class Program
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(Program));

        private static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            XmlConfigurator.Configure();

            if (Environment.UserInteractive)
            {
                var service = new DataSyncServer();
                service.StartService();
                Console.WriteLine("Press any key to stop program");
                Console.Read();
                service.StopService();
            }
            else
            {
                ServiceBase.Run(new ServiceBase[]
                {
                    new DataSyncServer()
                });
            }
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            try
            {
                Log.Error("", e.ExceptionObject as Exception);
            }
            catch (Exception)
            {
                // ignored
            }
        }
    }
}