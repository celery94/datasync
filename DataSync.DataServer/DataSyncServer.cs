﻿using log4net;
using System.Configuration;
using System.ServiceProcess;
using System.Web.Http;
using System.Web.Http.SelfHost;

namespace DataSync.DataServer
{
    internal partial class DataSyncServer : ServiceBase
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(DataSyncServer));
        private HttpSelfHostServer _apiServer;

        public DataSyncServer()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            var url = $"http://localhost:{ConfigurationManager.AppSettings["ApiPort"] ?? "30500"}";
            var config = new HttpSelfHostConfiguration(url);

            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                "API",
                "api/{controller}/{id}",
                new { id = RouteParameter.Optional });

            config.MaxReceivedMessageSize = 65536000;


            _apiServer = new HttpSelfHostServer(config);
            _apiServer.OpenAsync();
            Log.Info($"API server has been started with url:{url}.");
        }

        protected override void OnStop()
        {
            _apiServer.Dispose();
        }

        public void StartService()
        {
            OnStart(null);
        }

        public void StopService()
        {
            OnStop();
        }
    }
}