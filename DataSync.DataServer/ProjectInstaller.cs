﻿using System.ComponentModel;
using System.Configuration.Install;

namespace DataSync.DataServer
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }
    }
}