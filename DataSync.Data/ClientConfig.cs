﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace DataSync.Data
{
    [XmlRoot(ElementName = "config")]
    public class ClientConfig
    {
        [XmlElement("client")]
        public List<SyncClient> Clients { get; set; }
    }

    public class SyncClient
    {
        [XmlAttribute("tablename")]
        public string TableName { get; set; }

        [XmlAttribute("tablekey")]
        public string TableKey { get; set; }

        [XmlAttribute("clientId")]
        public string ClientId { get; set; }

        [XmlElement("item")]
        public List<MapItem> Items { get; set; }

        [XmlIgnore]
        public string SqlCmd
        {
            get
            {
                var cmd = new StringBuilder();

                cmd.Append("SELECT top 1000 ");
                Items.ForEach(q => cmd.Append(q.SqlCmd));
                cmd.Remove(cmd.Length - 1, 1);
                cmd.Append($" FROM {TableName} WHERE {TableKey} > {LastKey} ORDER BY {TableKey}");

                return cmd.ToString();
            }
        }

        [XmlIgnore]
        public string InsertCmd
        {
            get
            {
                var tableName = ConfigurationManager.AppSettings["ServerTableName"];

                var cmd = new StringBuilder();

                cmd.Append($"INSERT INTO [dbo].[{tableName}]({string.Join(",", Items.Select(q => q.Key))}) VALUES");
                cmd.Append($"({string.Join(",", Items.Select(q => "'" + q.ClientValue + "'"))})");

                return cmd.ToString();
            }
        }

        [XmlIgnore]
        public string KeyMapName => Items.Where(q => q.Value == TableKey).Select(q => q.Key).FirstOrDefault();

        [XmlIgnore]
        public int LastKey { get; set; }
    }

    public class MapItem
    {
        [XmlAttribute("key")]
        public string Key { get; set; }

        [XmlText]
        public string Value { get; set; }

        [XmlIgnore]
        public string ClientValue { get; set; }

        [XmlIgnore]
        public string SqlCmd
        {
            get
            {
                return $"{Value} AS {Key},";
            }
        }
    }
}