﻿using System;

namespace DataSync.Data
{
    public class OrderDetail
    {
        public int OrderID { get; set; }
        public int Record { get; set; }
        public string OrderNum { get; set; }
        public DateTime TTime { get; set; }
        public decimal Bin { get; set; }
        public decimal Isc { get; set; }
        public decimal Uoc { get; set; }
        public decimal FF { get; set; }
        public decimal EFF { get; set; }
        public decimal Rsh { get; set; }
        public decimal IRev2 { get; set; }
        public decimal Rs { get; set; }
        public decimal Tcell { get; set; }
        public decimal E { get; set; }
        public string Remark { get; set; }
        public string LNum { get; set; }
    }
}