﻿using log4net;
using System;
using System.Configuration;
using System.ServiceProcess;
using System.Threading;

namespace DataSync.DataClient
{
    internal partial class DataSyncClient : ServiceBase
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(DataSyncClient));
        private Timer _timer;

        public DataSyncClient()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                int.TryParse(ConfigurationManager.AppSettings["SyncTimer"], out var period);
                _timer = new Timer(SyncClientApp.Sync, null, 0, period == 0 ? 10000 : period * 1000);

                Log.Info($"Service Start with time period (second):{period}.");
            }
            catch (Exception ex)
            {
                Log.Error("", ex);
            }
        }

        protected override void OnStop()
        {
            _timer?.Dispose();
            Log.Info($"Service stoped.");
        }

        public void StartService()
        {
            OnStart(null);
        }

        public void StopService()
        {
            OnStop();
        }
    }
}