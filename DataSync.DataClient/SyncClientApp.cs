﻿using Dapper;
using DataSync.Data;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.Data.OleDb;
using System.Linq;
using System.Net.Http;

namespace DataSync.DataClient
{
    public class SyncClientApp
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(SyncClientApp));

        private static readonly ConcurrentDictionary<string, bool> RuningStatus =
            new ConcurrentDictionary<string, bool>();

        public static OleDbConnection OpenConnection()
        {
            var connection = new OleDbConnection(ConfigurationManager.ConnectionStrings["DbConn"].ConnectionString);
            connection.Open();
            return connection;
        }

        public static void Sync(object obj)
        {
            var clientId = ConfigurationManager.AppSettings["ClientId"] ?? "";

            Log.Info($"Client id:{clientId} read.");

            try
            {
                var serverAddr = ConfigurationManager.AppSettings["ServerAddr"] ?? "localhost:30500";

                if (RuningStatus.ContainsKey(clientId))
                {
                    if (RuningStatus[clientId])
                    {
                        Log.Info($"This client:{clientId} was running!!!");
                        return;
                    }
                }

                RuningStatus.AddOrUpdate(clientId, true, (key, oldValue) => true);

                string cmd = "";
                using (var httpClient = new HttpClient())
                {
                    try
                    {
                        var url = $"http://{serverAddr}/api/clients/{clientId}";

                        var result = httpClient.GetStringAsync(url).Result;
                        var client = JsonConvert.DeserializeObject<SyncClient>(result);

                        cmd = client.SqlCmd;

                        Log.Info($"Get sql cmd from server url:{url}, sql cmd:{cmd}.");
                    }
                    catch (Exception ex)
                    {
                        Log.Error("Get client config error.", ex);
                        throw;
                    }
                }

                IEnumerable<dynamic> orderDetails;
                using (var conn = OpenConnection())
                {
                    orderDetails = conn.Query(cmd);

                    Log.Info($"Get data from access, count:{orderDetails.Count()}");
                }

                if (orderDetails.Any())
                {
                    using (var httpClient = new HttpClient())
                    {
                        var url = $"http://{serverAddr}/api/clients/{clientId}";

                        var postResult = httpClient.PostAsync(url, orderDetails.AsJson()).Result;
                        Log.Info($"POST to server status code:{postResult.StatusCode}");
                    }
                }

                RuningStatus.AddOrUpdate(clientId, false, (key, oldValue) => false);
            }
            catch (Exception ex)
            {
                Log.Error("", ex);
                RuningStatus.AddOrUpdate(clientId, false, (key, oldValue) => false);
            }
        }
    }
}